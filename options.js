var postcode = window.location.search.substring("?postcode=".length);

chrome.storage.sync.set({"landregistryload": true}, null);
chrome.storage.sync.set({"landregistrypostcode": postcode}, null);

chrome.tabs.create({ url: "https://eservices.landregistry.gov.uk/mapsearch/"});
